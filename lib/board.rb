class Board
  attr_reader :grid

  def initialize(grid=self.class.default_grid)
    @grid = grid
  end

  def count
    @grid.flatten.count(:s)
  end

  def full?
    @grid.flatten.count(nil).zero?
  end

  def attack(pos)
    @grid[pos[0]][pos[1]] = :x
  end

  def won?
    count.zero?
  end

  def empty?(pos = proc { return count.zero? }.call)
    !@grid[pos[0]][pos[1]]
  end

  def place_random_ship
    raise_error if full?
    pos = [rand(@grid.size), rand(@grid[0].size)]
    pos = [rand(@grid.size), rand(@grid[0].size)] until empty?(pos)
    @grid[pos[0]][pos[1]] = :s
  end

  def [](pos)
    @grid[pos[0]][pos[1]] # because rspec?
  end

  def display
    puts ' '
    visual = graphic
    puts visual[0].join(' | ')
    n = -1
    visual.each { |x| puts " #{n} | #{x.join(' | ')}" unless n < 0; n += 1 }
  end

  private

  def graphic
    visual = []
    @grid.each { |x| visual << x.dup }
    visual.each { |x| x.map! { |n| n == :x ? 'X' : 'O' } }
    row1 = ['  ']
    visual.each_index { |i| row1 << i }
    visual.unshift(row1)
  end

  def self.default_grid
    default = []
    10.times { default << [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil] }
    default
  end

end
