class HumanPlayer

  def get_play
    puts 'Enter your move as row, column (e.g. 1,2)'
    gets.chomp.split(',').map(&:to_i)
  end

end
