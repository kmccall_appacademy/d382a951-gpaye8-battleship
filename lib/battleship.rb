require_relative 'board.rb'
require_relative 'player.rb'
require_relative 'battleship.rb'

class BattleshipGame
  attr_reader :board
  def initialize(player, board)
    @player = player
    @board = board
  end

  def attack(pos)
    @board.attack(pos)
  end

  def setup
    10.times { @board.place_random_ship }
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play_turn
    @board.display
    attack(@player.get_play)
    @board.display
  end

end

if __FILE__ == $PROGRAM_NAME
  game = BattleshipGame.new(HumanPlayer.new, Board.new)
  game.setup
  game.play_turn until game.game_over?
end
